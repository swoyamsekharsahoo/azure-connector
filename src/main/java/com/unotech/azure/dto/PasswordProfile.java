package com.unotech.azure.dto;

public class PasswordProfile {

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
