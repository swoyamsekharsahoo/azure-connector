package com.unotech.azure.service;

public interface AuthTokenService {
	
	String getToken();

	String getUrl();
}
